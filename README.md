event-amd
=========

This is a AMD copy of the nodejs event

API can be found here: http://nodejs.org/api/events.html

I have also included a PHP and Python implementation of the eventListener. I will work on making them behave in the same manner. The goal will be to create components that will be have the same. Currently I want to create a Rabbitmq layer that is the same so that it is easily compatible to comunicate between services. I hope to expand this to do more communication between different services so that I can utilize the best service to do different jobs.
=======
# event_amd

## Introduction
This is a amd verision of the events found in nodejs used for clients through requirejs
