<?php 

class EventEmitter{
  protected $events = array();
  protected $defaultMaxListeners = 10;
  protected $_maxListeners;
  public function __construct(){
    $this->setMaxListeners( $this->defaultMaxListeners );
  }

  public function setMaxListeners( $n ){
    if( is_int($n) ){
      $this->_maxListeners = $n;
    } 
  }

  public function emit( $type ){
    $arg_num = func_num_args();
    if( !is_array( $this->events ) ){
      $this->events = array();
    }
    //TODO: Error Handler
    if( isset( $type ) && array_key_exists( $type, $this->events )){
      $handler = $this->events[ $type ];
      
      $arg_list = func_get_args();
      array_shift( $arg_list );
      if( is_array($handler) ){
	foreach(  $handler as $listener ){
	  call_user_func_array( $listener, $arg_list );
	}
      } else if( is_callable( $handler ) ){
	call_user_func_array( $handler, $arg_list );
      }
    }else{
      return false;
    }
  }

  public function addListener( $type, $listener ){
    if( !is_array( $this->events ) ){
      $this->events = array();
    }
    if( array_key_exists( $type, $this->events )){
      $listeners = $this->events[$type];
      if( is_callable( $listeners ) ){
	$this->events[$type] = array( $listeners, $listener );
      }else{
	$listeners[]=$listener;
	if( count($listeners) > $this->_maxListeners ){
	  error_log('(PHP) warning: possible EventEmitter memory leak detected. '
		    .count($listeners).' listeners added. '+$this->_maxListeners. ' max.'
		    .'Use emitter.setMaxListeners() to increase limit.'
		    );
	}
	$this->events[$type] = $listeners;
      }
    }else{
      $this->events[$type] = $listener;
    }
  }

  public function on( $type, $listener ){
    $this->addListener( $type, $listener );
  }
  
  public function removeListener( $type, $listener ){
    if( !is_array( $this->events ) ){
      $this->events = array();
    }
    if( array_key_exists( $type, $this->events )){
      $handler = $this->events[$type];
      if( is_array($handler) ){
	foreach( $handler as $fn_idx => $fn_candidate ){
	  if( $fn_candidate == $listener ){
	    unset( $handler[$fn_idx] );
	    $this->events[$type] = $handler;
	    print "Found Match {$fn_idx}\n";
	    print_r( $this->events );
	  }
	}
	//$new_handler = array_diff( $handler, array( $listener) );
	//print_r( $handler );
      }else{
	unset( $this->events[$type] );
      }
    }
    
  }

}

?>