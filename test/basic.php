<?php

require('../vendor/autoload.php');

$listener = new EventEmitter();

/*
$listener->on( 'hello', function( $data, $callback ){
    print "Callback\n";
  });
$listener->on( 'hello', function( $data, $callback ){
    print "Callback 2\n";
  });
$listener->on( 'hello', function( $data, $callback ){
    print "Callback !!\n";
  });
*/
$remove_fn = function( $data, $callback ){
  print "Callback 3\n";
  $callback();
};

$listener->on( 'hello', $remove_fn );
$listener->emit('hello', 'second_arg', function(){
    print "emit_callback\n";
});

$listener->on( 'goodbye', function( ){
    print "Goodbye world\n";
  });

$listener->removeListener('hello', $remove_fn );

$listener->emit('hello', 'second_arg', function(){
    print "emit_callback second time trhough\n";
});

$listener->emit('goodbye');

?>