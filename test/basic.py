#! /usr/bin/env python
#        \file basic.py
#  
#        \author borrey
# 
#        Date Created: 2013-06-12T14:29:47-0600\n
#        Date Modified:
# 
#        Copyright  All Rights Reserved
if __name__ == '__main__':
    import sys, os
    sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
    from  event_amd import EventEmitter
    listener = EventEmitter()
    def event_handler( data = {}, callback = None) :
        print 'one'
        print data
        if callback is not None :
            callback()
    def event_handler_two( data = {}, callback = None) :
        print 'two'
        print data
        if callback is not None :
            callback()
    def event_handler_three( data = {}, callback = None) :
        print 'three'
        print data
        if callback is not None :
            callback()
    def event_callback():
        print 'done'

    listener.on('hello', event_handler )
    listener.on('hello', event_handler_two )
    listener.on('hello', event_handler_three )
    listener.removeListener('hello', event_handler )
    listener.removeListener('hello', event_handler_two )
    listener.removeListener('hello', event_handler_three )
    listener.emit( 'hello','second_arg', event_callback )
